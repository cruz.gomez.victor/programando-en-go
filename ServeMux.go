package main

import (
	"fmt"
	"net/http"
)

//ServeMux, es un enrutador que maneja las peticiones http
func main() {
	//Crear un serverMux
	mux:=http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, r*http.Request){
		fmt.Fprintf(w, "<h1>Hola mundo</h1>")
	})

	mux.HandleFunc("/prueba", func(w http.ResponseWriter, r*http.Request){
		fmt.Fprintf(w, "<h1>Hola mundo desde prueba</h1>")
	})

	mux.HandleFunc("/usuario", func(w http.ResponseWriter, r*http.Request){
		fmt.Fprintf(w, "<h1>Hola mundo desde usuario</h1>")
	})

	http.ListenAndServe(":8082", mux)
}
