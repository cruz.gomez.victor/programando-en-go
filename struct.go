package main

//Un struct, es un tipo de datos compuestos
/*type persona struct {
	nombre   string
	apellido string
	edad     int
}

func main() {
	p1 := persona{
		nombre:   "Victor",
		apellido: "Cruz",
		edad:     10,
	}

	p2 := persona{
		nombre:   "Grace",
		apellido: "Cruz",
		edad:     20,
	}

	fmt.Println(p1)
	fmt.Println(p2)

	fmt.Println(p1.nombre, p1.apellido)
	fmt.Println(p2.nombre, p2.apellido)
	fmt.Println(p1.edad)
}
*/

//-------------Struc embebidos-------------
//Una propiedad de un struct va a ser otro struct
//Las propiedad se promueven (heredan)
/*
type persona struct {
	nombre   string
	apellido string
	edad     int
}

type agenteSecreto struct {
	persona
	lpm bool
}

func main() {
	as1 := agenteSecreto{
		persona: persona{
			nombre:   "victor",
			apellido: "Cruz",
			edad:     33,
		},
		lpm: true,
	}

	fmt.Println(as1)
	fmt.Println(as1.nombre)
	fmt.Println(as1.persona.apellido)
}
*/

//-------------Struc anonimos-------------
//Anonimo, algo al cual no conocemos la identidad
//p1:=struct{} (es un literal compuesto)
//Los struct son utilizados para algo especifico y para no ensuciar el codigo
//Se recomienda usar struct como nuevos tipos.

/*
Es go un lenguaje orientado a objetos?
-Provee interfaces mas facil de utilizar.
-En go no hay clases. Todos son tipos. Los struct son lo analogos a clases.
-En go no existen los modificadore public, private, friendly
-En go no se crean instancias se crear un valor del tipo.

1.Encapsulacion
-Estado (campos)
-Comportamientos(metodos)
-Visible

2.Reusabilidad
-herencia (tipos embebidos). Ej. Persona como tipo de dato

3.Polimorfismo
-Interfaces

4.Overriding
-Promocion

*/

/*
type Persona struct {
	nombre    string
	apellidos string
	edad      int
}

func main() {
	p1 := Persona{
		nombre:    "Victor",
		apellidos: "Cruz",
		edad:      33,
	}

	p2 := struct {
		nombre    string
		apellidos string
		edad      int
	}{
		nombre:    "grace",
		apellidos: "cruz",
		edad:      7,
	}

	fmt.Println(p1)
	fmt.Println(p2)
}
*/
