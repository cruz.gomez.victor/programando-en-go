package main

import (
	"os"
	"text/template"
)

//Toda la data se renderiza desde el lado del servidor. El cliente ya recibe toda la data procesada.
//Templates en GO para crear paginas web tradicional.
//Renderizar desde el servidor.
//text template, texto plano.
//html template, parse al codigo a html.

type Persona struct {
	Nombre string
	Edad   int
}

const tp = `Nombre: {{.Nombre}} Edad: {{.Edad}}`

func main() {
	persona := Persona{"Victor", 30}
	t := template.New("persona") // Devuelve un puntero del tipo template
	t, err := t.Parse(tp)//

	if err != nil {
		panic(err)
	}

	err = t.Execute(os.Stdout, persona) //Execute, ejecuta el template. OS.Stdout imprime en la consola.
	if err != nil {
		panic(err)
	}
}
