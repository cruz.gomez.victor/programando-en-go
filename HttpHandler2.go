package main

import (
	"fmt"
	"net/http"
)

//En go cualquier objeto puede ser un manejador al implementar la interfaz handler

func holaMundo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola mundo</h1>")
}

func prueba(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola des prueba</h1>")
}

func usuario(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola desde usuario</h1>")
}

type mensaje struct{
	msg string
}

//La estructura puede funcionar como manejardor porque
//implementa serveHTTP
func (m mensaje) ServeHTTP(w http.ResponseWriter, r *http.Request){
	fmt.Fprintln(w, m.msg)
}

func main() {
	msg:= mensaje{msg:"hola mundo de nuevo"}

	mux := http.NewServeMux()
	mux.HandleFunc("/", holaMundo)
	mux.HandleFunc("/prueba", prueba)
	mux.HandleFunc("/usuario", usuario)

	mux.Handle("/hola", msg)

	http.ListenAndServe(":8084", mux)
}
