package main

import "fmt"

//------------Ejercicio 1------------
/*func main() {
	arreglo := [5]int{0, 1, 2, 3, 4}
	fmt.Println(arreglo)

	for indice, valor := range arreglo {
		fmt.Println(indice, valor)
	}
}*/

//------------Ejercicio 2------------
//Para que sea slice hay que quiter la cantidad de elementos
/*func main() {
	slicedd := []int{0, 1, 2, 3, 4, 10, 11, 12, 13, 14}

	for indice, valor := range slicedd {
		fmt.Println(indice, valor)
	}

	fmt.Printf("%T\n", slicedd)
}*/

//------------Ejercicio 3------------
/*func main() {
	slice := []int{10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 21}

	fmt.Println(slice[:4])
	fmt.Println(slice[4:8])
	fmt.Println(slice[8:12])
	fmt.Println(slice)
}*/

//------------Ejercicio 4------------
/*func main() {
	x := []int{42, 43, 44, 45, 46, 47, 48, 49, 50, 51}

	x = append(x, 52)
	for indice, valor := range x {
		fmt.Println(indice, valor)
	}
	x = append(x, 53, 54, 55)
	for indice, valor := range x {
		fmt.Println(indice, valor)
	}

	y := []int{56, 57, 58, 59, 60}

	x = append(x, y...)

	fmt.Println(x)
}*/

//------------Ejercicio 5------------
/*func main() {
	x := []int{42, 43, 44, 45, 46, 47, 48, 49, 50, 51}
	y := append(x[:3], x[6:]...)
	fmt.Println(y)
}*/

//------------Ejercicio 6------------
/*func main() {
	x := make([]string, 9, 9)
	x = []string{`La Paz`, `Oruro`, `Potosi`, `Tarija`, `Sucre`, `Santa Cruz`, `Beni`, `Pando`, `Cochabamba`}
	fmt.Println(cap(x))
	fmt.Println(len(x))

	for i := 1; i <= len(x); i++ {
		fmt.Println(i, x[i-1])
	}
}*/

//------------Ejercicio 7------------
/*func main() {
	slicex := []string{"Batman", "Jefe", "Vestigo de negro"}
	slicey := []string{"Robin", "Ayudante", "Vestido de colores"}

	slicexy := [][]string{slicex, slicey}
	fmt.Println(slicexy)

	for indice, valor := range slicexy {
		//fmt.Println(indice, valor)
		fmt.Printf("El indice es:%v y el valor es: %v\n", indice, valor)
		for indicei, valori := range valor {
			fmt.Printf("\t %v , %v\n", indicei, valori)
		}
	}
}*/

//------------Ejercicio 8-----------
func main() {
	/*	personas := map[string][]string{
			"victor": {`computadoras`, `mujeres`, `cerveza`},
			"maria":  {`tv`, `varones`, `wisky`},
			"grace":  {`tv`, `ninos`, `dibujos`},
		}

		for clave, valor := range personas {
			fmt.Println("Registro: ", clave)
			for indice, val := range valor {
				fmt.Println("\t", indice, val)
			}
		}
	*/
	//Version dos
	personas2 := map[string][]string{
		`victor`: []string{`computadoras`, `mujeres`, `cerveza`},
		`maria`:  []string{`tv`, `varones`, `wisky`},
		`grace`:  []string{`tv`, `ninos`, `dibujos`},
	}

	personas2[`jhon`] = []string{`futbol`, `ninas`, `balon`}

	delete(personas2, `victor`)

	for clave, valor := range personas2 {
		fmt.Println("Registro: ", clave)
		for indice, val := range valor {
			fmt.Println("\t", indice, val)
		}
	}

}
