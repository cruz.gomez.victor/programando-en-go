package main

import (
	"fmt"
	"sort"
)

//------------------Ejercicio practico 2
/*
type persona struct {
	Nombre    string `json:"nombre"`
	Apellidos string `json:"apellidos"`
	Edad      int    `json:"edad"`
}

func main() {
	//El objeto json si o si entre ``
	s := `[{"nombre":"Victor","apellidos":"Cruz Gomez","edad": 30}]`

	//Tener cuidado si el json tiene una lista de objetos entonces recuperalo en un slice.
	var slicePersonas []persona
	fmt.Println(slicePersonas)

	//fmt.Println(&personas)

	err := json.Unmarshal([]byte(s), &slicePersonas)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(slicePersonas)

	for _, persona := range slicePersonas {
		fmt.Println(persona.Nombre)
		fmt.Println(persona.Edad)
	}

}
*/

//------------------Ejercicio practico 3
/*
type usuario struct {
	Nombre string
	Edad   int
}

func main() {
	u1 := usuario{
		Nombre: "victor",
		Edad:   30}

	u2 := usuario{
		Nombre: "victor",
		Edad:   30}

	usuarios := []usuario{u1, u2}

	println("...imprimiendo el json codificado directo al cable")
	error := json.NewEncoder(os.Stdout).Encode(usuarios)
	if error != nil {
		fmt.Println("Ocurrio un error")
		fmt.Println(error)
	}

}
*/

//------------------Ejercicio practico 4
/*
func main() {
	numeros := []int{3, 5, 1, 2, 4}
	nombres := []string{"victor", "ana", "juan"}

	fmt.Println(numeros)
	fmt.Println(nombres)

	sort.Ints(numeros)
	sort.Strings(nombres)
	fmt.Println(numeros)
	fmt.Println(nombres)
}
*/

//------------------Ejercicio practico 5
type Persona struct {
	Nombre string
	Edad   int
}

type PorEdad []Persona

func (a PorEdad) Len() int           { return len(a) }
func (a PorEdad) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a PorEdad) Less(i, j int) bool { return a[i].Edad < a[j].Edad }

func main() {
	personas := []Persona{
		Persona{Nombre: "victor", Edad: 50},
		Persona{Nombre: "Juan", Edad: 40},
		Persona{Nombre: "Juali", Edad: 30},
	}
	fmt.Println("Antes de ordenar")
	fmt.Println(personas)
	sort.Sort(PorEdad(personas))
	fmt.Println("...ordenando")
	fmt.Println(personas)
}
