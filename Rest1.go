package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type Note struct {
	//Anotacion de campos
	Title       string    `json:"title"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"create_at"`
}

var noteStore = make(map[string]Note)
var id int

//GETNoteHandle - GET - /api/notes
func GETNoteHandler(w http.ResponseWriter, r *http.Request) { //Recibe dos parametros
	var notes []Note              //slice de notas
	for _, v := range noteStore { //Recorrer el mapa
		notes = append(notes, v) //agragando al slide
	}
	//Se devolvera un contenido del tipo json
	//Establer el contenido del tipo json
	w.Header().Set("Content-Type", "application/json")
	//Paquete json de Go
	//En el objeto j se tiene el slide de nota en formato json
	j, err := json.Marshal(notes) //codificar en json
	if err != nil {
		panic(err)
	}

	//Respuesta de la cabecera
	w.WriteHeader(http.StatusOK)
	//w.WriteHeader(200)
	//Pasamos el cuerpo
	w.Write(j)
}

//PostNoteHandler - post - /api/notes
func POSTNoteHandler(w http.ResponseWriter, r *http.Request) { //Recibe dos parametros
	//slice de notas
	var note Note
	//Decodifique el json en note
	err := json.NewDecoder(r.Body).Decode(&note)
	if err != nil {
		panic(err)
	}
	note.CreatedAt = time.Now()
	id++
	//Convertir un entero a string
	k := strconv.Itoa(id)
	noteStore[k] = note

	w.Header().Set("Content-Type", "application/json")
	j, err := json.Marshal(note)
	if err != nil {
		panic(err)
	}
	w.WriteHeader(http.StatusCreated)
	w.Write(j)
}

//PutNoteHandler - PUT - /api/notes
func PUTNoteHandler(w http.ResponseWriter, r *http.Request) {
	//Vars, extrae las diferentes variables y devuelve un slice con las variables extraidas
	vars := mux.Vars(r)
	k := vars["id"] //index nombre de la variable
	var noteUpdate Note
	err := json.NewDecoder(r.Body).Decode(&noteUpdate)
	if err != nil {
		panic(err)
	}
	//Ejecutar una sentencia antes de que el if haga su comprobacion
	//S
	if note, ok := noteStore[k]; ok {
		noteUpdate.CreatedAt = note.CreatedAt
		delete(noteStore, k)
		noteStore[k] = noteUpdate
	} else {
		log.Printf("No encontramos el id %s", k)
	}
	w.WriteHeader(http.StatusNoContent) //204
}

//DeleteNoteHandler - DELETE - /api/notes
func DELETENoteHandler(w http.ResponseWriter, r *http.Request) {
	//Vars, extrae las diferentes variables y devuelve un slice con las variables extraidas
	vars := mux.Vars(r)
	k := vars["id"] //index nombre de la variable

	if _, ok := noteStore[k]; ok {
		delete(noteStore, k)
	} else {
		log.Printf("No encontramos el id %s", k)
	}
	w.WriteHeader(http.StatusNoContent) //204
}

func main() {
	//Crear un enrutador
	r := mux.NewRouter().StrictSlash(false)
	r.HandleFunc("/api/notes", GETNoteHandler).Methods("GET")
	r.HandleFunc("/api/notes", POSTNoteHandler).Methods("POST")
	r.HandleFunc("/api/notes/{id}", PUTNoteHandler).Methods("PUT")
	r.HandleFunc("/api/notes/{id}", DELETENoteHandler).Methods("DELETE")

	server := &http.Server{
		Addr:           ":8080", //escuchando en el puerto
		Handler:        r,       //pasando el manejador
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	log.Println("Listening http://localhost:8080")
	server.ListenAndServe()
}
