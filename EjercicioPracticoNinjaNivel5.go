package main

import "fmt"

//------------------Ejercicio 1----------------
/*
type Persona struct {
	nombre   string
	apellido string
	helados  []string
}

func main() {
	p1 := Persona{
		nombre:   "Victor",
		apellido: "Cruz",
		helados:  []string{"vainilla", "cereza", "fresa"},
	}

	p2 := Persona{
		nombre:   "Victor2",
		apellido: "Cruz2",
		helados:  []string{"vainilla", "cereza", "fresa"},
	}

	fmt.Println(p1.nombre)
	fmt.Println(p1.apellido)

	fmt.Println(p2.nombre)
	fmt.Println(p2.apellido)

	for indice, valor := range p1.helados {
		fmt.Println("\t", indice, valor)
	}
}
*/

/*
type Persona struct {
	nombre   string
	apellido string
	helados  []string
}

func main() {
	p1 := Persona{
		nombre:   "Victor",
		apellido: "Cruz",
		helados:  []string{"vainilla", "cereza", "fresa"},
	}

	p2 := Persona{
		nombre:   "Juan",
		apellido: "Cruz",
		helados:  []string{"vainilla", "cereza", "fresa"},
	}

	fmt.Println(p1.nombre)
	fmt.Println(p1.apellido)

	for indice, valor := range p1.helados {
		fmt.Println("\t", indice, valor)
	}

	mapa := map[string]Persona{
		"victor": Persona{
			nombre:   p1.nombre,
			apellido: p1.apellido,
			helados:  p1.helados,
		},
		"juan": Persona{
			nombre:   p1.nombre,
			apellido: p1.apellido,
			helados:  p1.helados,
		},
	}

	fmt.Println(mapa)

	for clave, valor := range mapa {
		fmt.Println("-", clave)
		for indice, val := range valor.helados {
			fmt.Println("\t", indice, val)
		}
	}

	fmt.Println("---------------------------")
	m := map[string]Persona{
		p1.nombre: p1,
		p2.nombre: p2,
	}

	for _, valor := range m {
		fmt.Println(valor.nombre)
		fmt.Println(valor.apellido)
		for i, v := range valor.helados {
			fmt.Println(" ", i, v)
		}
	}
}
*/

//------------------Ejercicio 3----------------
/*
type vehiculo struct {
	puertas int
	color   string
}

type camion struct {
	cuatroRuedas bool
	vehiculo
}

type sedan struct {
	lujoso bool
	vehiculo
}

func main() {
	c := camion{
		cuatroRuedas: true,
		vehiculo: vehiculo{
			puertas: 4,
			color:   "rojo",
		},
	}

	fmt.Println(c)

	s := sedan{
		lujoso: true,
		vehiculo: vehiculo{
			puertas: 5,
			color:   "azul",
		},
	}

	fmt.Println(s)
}
*/

//------------------Ejercicio 4----------------
//Los struct son utilizados para algo especifico. Tambien se utilizan para no ensuciar el codigo

func main() {
	persona := struct {
		nombre    string
		apellidos string
		edad      int
	}{
		nombre:    "Victor",
		apellidos: "Cruz Gomez",
		edad:      33,
	}

	fmt.Println(persona.nombre)

	s := struct {
		nombre  string
		amigos  map[int]string
		bebidas []string
	}{
		nombre: "Victor Cruz Gomez",
		amigos: map[int]string{
			10: "Victor",
			20: "Maria",
			30: "Grace",
		},
		bebidas: []string{
			"fanta", "coca cola", "pespi",
		},
	}

	fmt.Println("-----------Datos completos de la personas-----------")
	fmt.Println(s.nombre)
	fmt.Println("-----------Amigos")
	for c, v := range s.amigos {
		fmt.Println("\t", c, v)
	}
	fmt.Println("-----------Bebidas")
	for i, v := range s.bebidas {
		fmt.Println("\t", i, v)
	}
}
