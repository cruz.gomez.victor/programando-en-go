package main

import "fmt"

/*
-Todos los valores creados son almacenados en memoria ram.
-Cada hubicacion en memoria tiene una direccion.
-Los punteros () son un tipo de variable especial que contiene la direccion de memoria de una dato o otra variable.
-Los punteros son un nuevo tipo de dato que almacenan una direccion de memoria.
-El puntero no conocer cual es el valor de la direccion de memoria, sino el contenido.
-Todo puntero almacena una direccion de memoria
-& = direccion de
-* = apuntado por
*/

/*
func main() {
	a := 42
	fmt.Println(a)
	fmt.Println(&a) //Ver la direccion de momoria

	fmt.Printf("%T\n", a)
	fmt.Printf("%T\n", &a)

	var b *int = &a
	fmt.Println(b)
	fmt.Println(*b)

	*b = 43
	fmt.Println(a)
}
*/

/*
Cuando usar punteros?
-Si tenemos una variable grande de datos. es mas eficiente pasar la direccion.
-Cambiar directamente un valor de una direccion de memoria
-Todo en Go es pasado por valor. Lo que ves es lo que obtienes
-Es mejor copiar un puntero (direccion de memoria) antes que una variable que ocupa mucho espacio
*/

func main() {
	x := 0
	fmt.Println("x antes", x)
	fmt.Println("x antes", &x)
	foo(&x)
	fmt.Println("-----------")
	fmt.Println("x despues", x)
	fmt.Println("x despues", &x)
}

func foo(y *int) {
	fmt.Println("-----------")
	fmt.Println("y antes", y)
	fmt.Println("y antes", *y)
	*y = 30
	fmt.Println("y despues", y)
	fmt.Println("y despues", *y)
}
