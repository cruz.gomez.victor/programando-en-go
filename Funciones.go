package main

//-----------Defer
//Diferir en funciones.
//Diferir, dejar para un momento o fecha posterior. Ej diferir un viaje hasta el me proximo.

/*
func main() {
	defer foo()
	bar()
}

func foo() {
	fmt.Println("foo")
}

func bar() {
	fmt.Println("bar")
}

*/
//-----------Metodos
//Metodos adjuntados a los tipos. Es decir un tipo struct puede tener sus metodos asociados.
//func(r receptor)identificador(parametros)(retorno(s))}{codigo}

/*
type persona struct {
	nombre   string
	apellido string
}

type agenteSecreto struct {
	persona
	lpm bool
}

func (a agenteSecreto) presentarse() {
	fmt.Println("Hola, soy ", a.nombre, a.apellido)
}

func main() {
	as1 := agenteSecreto{
		persona: persona{
			nombre:   "victor",
			apellido: "curz",
		},
		lpm: true,
	}

	fmt.Println(as1)
	as1.presentarse()
}
*/

//-----------Interfaces y Polimorfismo
/*

Intefaces
-En go lo valores pueden ser mas de un tipo.
-

Polimorfismo
-


*/
