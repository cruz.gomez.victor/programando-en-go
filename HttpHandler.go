package main

import (
	"fmt"
	"net/http"
)

//En go cualquier objeto puede ser un manejador al implementar la interfaz handler

func holaMundo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola mundo</h1>")
}

func prueba(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola des prueba</h1>")
}

func usuario(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola desde usuario</h1>")
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", holaMundo)
	mux.HandleFunc("/prueba", prueba)
	mux.HandleFunc("/usuario", usuario)

	http.ListenAndServe(":8083", mux)
}
