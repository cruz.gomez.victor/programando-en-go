package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

//En go cualquier objeto puede ser un manejador al implementar la interfaz handler

func holaMundo(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola mundo</h1>")
}

func prueba(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola des prueba</h1>")
}

func usuario(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<h1>Hola desde usuario</h1>")
}

type mensaje struct{
	msg string
}

//La estructura puede funcionar como manejardor porque
//implementa serveHTTP
func (m mensaje) ServeHTTP(w http.ResponseWriter, r *http.Request){
	fmt.Fprintln(w, m.msg)
}

func main() {

	msg:= mensaje{msg:"hola mundo de nuevo"}

	mux := http.NewServeMux()

	//Handle para servir archivos
	//Todos los archivos dentro de la carpeta public
	//el servidor los va ha servir automaticamente
	//Es similar al servidor de apache "www" o "htdoc". Todos los archivos que estan dentro de la carpeta se siven
	fs:=http.FileServer(http.Dir("public"))
	mux.Handle("/", fs)
	mux.HandleFunc("/prueba", prueba)
	mux.HandleFunc("/usuario", usuario)
	mux.Handle("/hola", msg)




	//Creando la estructura server
	server:= &http.Server{
		Addr: ":8080",
		Handler:mux,
		ReadTimeout:10*time.Second, //Espere 10 segundo hasta que de la respuesta
		WriteTimeout:0*time.Second, //
		MaxHeaderBytes:1 << 20, //tamano maximo de la cabecera. multiplica 1 * 20 veces.
	}

	log.Println("Listening...")
	log.Fatal(server.ListenAndServe())



}
