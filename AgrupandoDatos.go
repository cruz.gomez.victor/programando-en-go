package main

import "fmt"

//-------------mapas------------
/*func main() {
	m := map[string]int{
		"batman": 32,
		"robin":  27,
	}
	fmt.Println(m)
	fmt.Println(m["batman"])
	fmt.Println(m["Victor"])

	v, ok := m["Victor"]
	fmt.Println(v)
	fmt.Println(ok)

	if v, ok := m["robin"]; ok {
		fmt.Println("Existe robin, felicidades:", v)
	}
}*/

//-------------mapas agregar elemento range------------
/*func main() {
	m := map[string]int{
		"robin":  10,
		"batman": 20,
	}

	fmt.Println(m)

	m["victor"] = 33

	for llave, valor := range m {
		fmt.Println(llave, valor)
	}
}*/

//-------------mapas agregar elemento range------------
func main() {
	m := map[string]int{
		"robin":  10,
		"batman": 20,
	}

	fmt.Println(m)

	delete(m, "robin")
	fmt.Println(m)

	if v, ok := m["batman"]; ok {
		fmt.Println("Se borro la lleve con valor ", v)
		delete(m, "batman")
	}

	fmt.Println(m)
}
