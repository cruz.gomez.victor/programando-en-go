package main

import (
	"fmt"
	"math"
)

/*
-2004 amd saca al mercado procesador de multiples nucleos.
-Dentro en google se inicia la discusion que de se necesita un lenguaje para los multiples nucleos.
-2009 anuncia formalmente. En 2012 lo liberar a Go
-Go esta disena para trabajar multiples nucleos.

Concurrencia y Paralelimos
Paralelismos
-Tiene varios procesos en ejecucion al mismo tiempo
-Con un solo nucleo solo se corre en un nucleo

Concurrencia
-Podemos lidear varias cosas al mismo tiempo.
-Pausar una tarea y ejecutar otro al mismo tiempo.
-Es un patron de diseno.
-Ejemplo: Escribir codigo concurrente como baile, deberia de existir alguien que sincronice a la gente que baila en una pista, eso es concurrencia.

Paquete runtime
-Obtiene la arquitectura donde correo el programa.
-Obtiene el numero de gorutinas
-Obtiene la canitdad de nucleos
*/

//--------Wait group
/*
var wg sync.WaitGroup

func main() {
	fmt.Println("os", runtime.GOOS)
	fmt.Println("arquitectura", runtime.GOARCH)
	fmt.Println("cpu", runtime.NumCPU())
	fmt.Println("Gorutinas", runtime.NumGoroutine())
	wg.Add(1) //Adicionar la cantidad de gorutinas del main
	go foo()  //Aqui foo() se ejecutar por otro lado
	fmt.Println("Gorutinas", runtime.NumGoroutine())
	bar()
	wg.Wait() //Esperar que todas las gorutinas terminen
}

func foo() {
	for i := 0; i < 10; i++ {
		fmt.Println("foo:", i)
	}
	wg.Done() //terminar la gorutina
}

func bar() {
	for i := 0; i < 10; i++ {
		fmt.Println("bar:", i)
	}
}
*/

//--------Method set
/*
-Add, done y wait reciben un puntero como parametro
-Waitgroup si no es un puntero como puedo llamar al metodo add, wait y done?

Method sets
-El metodo de un tipo determina la interfaz que el tipo implementa.
-Si tienes un T, puedes llamar a metodos que tiene un receptor *T
asi como a metodos que tienen un receptor T.

-*T => metodos con receptor *T y T
- T => metodos con receptor *T y T
- T no direccionable => metodos con receptor T

-Los metodos de una interfaz son estricto en lo receptores
-Las variables puede llamar a metodos con receptores *T o T
-El metodo set (variable.metodo()) determinar los receptores * o T
-T tiene una direccion en memoria se pueden llamar metdos con receptores *T

-En resumen se pueden llamar metodos que tengan un receptor *T si la variable TIENE UNA DIRECCION EN MOMORIA

*/


/*
type circulo struct {
	radio float64
}

type figura interface {
	area() float64
}

func (c *circulo) area() float64 {
	return math.Pi * c.radio * c.radio
}

func info(s figura) {
	fmt.Println("area", s.area())
}

func main() {
	c := circulo{radio: 5}
	//info(c) //Genera un error porque area() esta asosicado a *circulo
	info(&c) //Correcto, porque el metodo area() esta asociado a un puntero circulo por lo tanto recibe una referencia de la variable c
	//fmt.Println((&c).area()) //Tambien se pude llamar al metodo de esta forma
	fmt.Println(c.area()) //funciona porque c tiene una direccion en memoria.
}
*/


//--------Documentacion
/*
Goroutine
Una goroutine tiene un modelo simple: es una funcion ejecutandose concurrentemente con otras goroutines en el mimso espacio de memoria.
*/